#####################################################################################################################
#
# Script to create a location folder for WinDSX
# Each location that is added needs to have it's own WinDSX installation folder under E:\dsxSQL or E:\dsxMDB.
# 
# 
#
# Create on 2012-06-08 By Michael Pal
#
#####################################################################################################################

# Parent Folder location and masterOverwrite Switch can be passed at the command line.
# If no values are entered then the script will launch a browser window and ask for a location.
param ([string]$targetParentFolder, [bool]$masterOverwrite=$false)

switch ($masterOverwrite) {
	$true { break }
	$false { break }
	default { $masterOverwrite = $false }
}

# Toggle debugging information.
$debug = 0

# Function to output debugging info.
function debug {
	param ([string]$value)
	Write-Host "*DEBUG OUTPUT* $value"
}

# Function that will read in a file that contains a secure string hash and decrypt it to plain text.
function decryptFile {
	param ([string]$filename,[string]$key_file)
	$key = gc $key_file
	$data = gc $filename | ConvertTo-SecureString -key $key
	$bstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($data)
	$pstr = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstr)
	
	return $pstr
}

# Function to setup DSX local database
function databaseSetup {
	param ([string]$targetFolder)
	
	$user = decryptFile .\l7donguser2 .\key
	$pass = decryptFile .\l7donguser1 .\key
	$server = decryptFile .\l7donguser3 .\key
	
	$adOpenStatic = 3
	$adLockOptimistic = 3
	
	$dataSource = "$targetFolder\Sys.mdb"
	$selectQuery = "SELECT * FROM Databases WHERE ID=1"
	
	$dsn = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=$dataSource;"
	if ($debug -eq 1) {
		debug "DSN = $dsn"
	}
	
	$connection = new-object -ComObject ADODB.Connection
	$recordSet = new-object -comobject ADODB.Recordset
	
	$connection.Open($dsn)
	
	$recordSet.Open($selectQuery, $connection, $adOpenStatic, $adLockOptimistic)
	$recordSet.MoveFirst()
	$recordSet.fields.item("Name").Value = $server
	$recordSet.fields.item("Path2Data").Value = $targetFolder
	$recordSet.fields.item("SqlUser").Value = $user
	$recordSet.fields.item("SqlPw").Value = $pass
	
	$recordSet.Update()
	$recordSet.Close()
	$connection.Close()
	
	$recordSet = $null
	$connection = $null
	
}

# Function to select folder if no folder value is passed as a parameter
function selectFolder {
	$message = "Please Select or Create Folder to Setup"
	$path = "E:\dsxSQL"
	
	$object = New-Object -comObject Shell.Application
     
    $folder = $object.BrowseForFolder(0, $message, 0, $path)
	
	if ($folder -ne $null) {	
		return $folder.self.path
	} else {
		exit
	}
}

# Funciton to show a wait message for the designated number of secs
function waitTimer{
	param ([int]$seconds,[string]$msg="Waiting...")
	
	Write-Host $msg -nonewline
	for ($i=$seconds; $i -ge 1; $i--) {
		Write-Host " $i" -nonewline
		start-sleep -seconds 1
	}
	Write-Host "`n"
}

# Main function to copy files over and run the database setup function.
function copyWindsxFiles {
	param( [string]$source, [string]$targetParent, [bool]$overwrite )
	
	if ($debug -eq 1) {
		debug "Source = $source"
		debug "Target Parent = $targetParent"
		debug "SOverwrite = $overwrite"
	}
	
	$target = "$targetParent\WinDsx"
	
	if ($overwrite -eq $true) {
		Write-Host "Removing $target..." -nonewline
		C:\Windows\System32\cmd.exe /c rmdir "$target\Icons" 
		C:\Windows\System32\cmd.exe /c rmdir "$target\Images"
		C:\Windows\System32\cmd.exe /c rmdir "$target\Maps"
		C:\Windows\System32\cmd.exe /c rmdir "$target\Wav"
		remove-item $target -recurse -force
		Write-Host "Completed"
		Write-Host "Copying $source to $target..." -nonewline
		copy-item $source $targetParent -recurse -force
		Write-Host "Completed"
	} else {
		Write-Host "Copying $source to $target..." -nonewline
		copy-item $source $targetParent -recurse
		Write-Host "Completed"
	}
	
	Write-Host "Configuring WinDSX..." -nonewline
	databaseSetup $target
	Write-Host "Completed"
	
	Write-Host "Removing static resource folders..." -nonewline
	remove-item "$target\Icons" -recurse -force
	remove-item "$target\Images" -recurse -force
	remove-item "$target\Maps" -recurse -force
	remove-item "$target\Wav" -recurse -force
	Write-Host "Completed"
	
	cd E:
	cd $target
	
	Write-Host "Creating symbloic links to main resource folder..."
	C:\Windows\System32\cmd /c mklink /D $target\Icons C:\Windsx\Icons
	C:\Windows\System32\cmd /c mklink /D $target\Images C:\Windsx\Images
	C:\Windows\System32\cmd /c mklink /D $target\Maps C:\Windsx\Maps
	C:\Windows\System32\cmd /c mklink /D $target\Wav C:\Windsx\Wav
	
	cd C:
	
	Write-Host "Completed setting up $target!`n`n"
	Read-Host "Press Enter to exit..."
}

$sourceFolder = "C:\WinDSX"
if ($targetParentFolder -eq "") {
	$targetParentFolder = selectFolder
}
if ($targetParentFolder -ne "") {
	if ((test-path $targetParentFolder) -eq $false) {
		$targetParentFolder = selectFolder
	}
}
$targetFolder = "$targetParentFolder\WinDSX"
$targetExists = test-path "$targetFolder"

Write-Host "`n`n******************************************************"
Write-Host "*"
Write-Host "* Source `t= `t$sourceFolder"
Write-Host "* Destination `t= `t$targetFolder"
Write-Host "* OverWrite `t= `t$masterOverwrite"
Write-Host "*"
Write-Host "******************************************************`n`n"

if ($targetExists -eq $false) {
	copyWindsxFiles $sourceFolder $targetParentFolder $false
} else {
	$targetHasFiles = gci $targetFolder
	
	Write-Host "$targetFolder already exists."
	
	if ($targetHasFiles.Count -lt 1) {
		Write-Host "$targetFolder is empty.  Overwriting..."
		copyWindsxFiles $sourceFolder $targetParentFolder $true
	} else {
		if ($masterOverwrite -eq 1) {
			copyWindsxFiles $sourceFolder $targetParentFolder $true
		} else {
			Write-Host "$targetFolder is not empty.  It contains the current files:"
			ls $targetFolder
			DO {
				$answer = Read-Host "Would you like to overwrite these files? Y/N"
			} while (([string]::Compare($answer,"y",$true) -ne 0) -AND (([string]::Compare($answer,"n",$true) -ne 0)))
			if ([string]::Compare($answer,"y",$true) -eq 0) {
				copyWindsxFiles $sourceFolder $targetParentFolder $true
			}
		}
	}
}