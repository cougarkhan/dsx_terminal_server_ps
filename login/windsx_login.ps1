Import-Module PSTerminalServices

function launch_windsx {
	param([string]$exePath, [string]$exeName, [string]$userName)

	$app = start-process $exePath -passThru

	do { 
		if (!(gps -id $app.id)) {
			& "C:\Windows\System32\Shutdown" "/l", "/f"
			exit
		}
		sleep 1
	}
	until ($false -eq $true)
}

$dsx = [xml](gc "C:\drs\scripts\login\dsx_config.xml")
$clientTS = Get-TSCurrentSession
$ipClient = $clientTS.ClientIPAddress
$appLaunched = 0
$userName = (gci env:username).value

$ipList = $dsx.dsx.ip


$ipList | %{ 
	if ($ipClient -imatch $_.value) {
		$mapDrive = $_.mapdrive
		if ($mapdrive -eq 1) {
			& net "use", "i:", "\\bis-mdb-dsx.bussops.ubc.ca\comaccess"
		}
		launch_windsx $_.exe $_.exename $userName
		$appLaunched = 1
		break
	}
}

if ($appLaunched -eq 0) { 
	& "C:\Windows\System32\Shutdown" "/l", "/f"
	exit
}