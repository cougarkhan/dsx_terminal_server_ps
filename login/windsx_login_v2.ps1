Import-Module PSTerminalServices

function launch_windsx {
	param([string[]]$exePath)
	
	$apps = @()
	foreach ($exe in $exePath)
	{
		$apps += start-process -filepath $exe -passthru
	
	do 
	{
		if (!(gps -id $apps.id -ErrorAction SilentlyContinue)) 
		{ 
			start-process -filepath "C:\Windows\System32\Shutdown.exe" -argumentlist @("/l","/f")
			exit
		}
		sleep 1
	}
	until ($false -eq $true)
}

$dsx = [xml](gc "C:\drs\scripts\login\dsx_config.xml")
$clientTS = Get-TSCurrentSession
$ipClient = $clientTS.ClientIPAddress
$appLaunched = 0
$userName = (gci env:username).value

$ipList = $dsx.dsx.ip

foreach ($ip in $ipList)
{
	if ($ipClient -imatch $ip.value) 
	{
		$mapDrive = $ip.mapdrive
		if ($mapdarive -eq 1)
		{
			start-process -filepath "C:\Windows\System32\net.exe" -arguementlist @("use","i:","\\bis-mdb-dsx.bussops.ubc.ca\comaccess")
		}
		launch_windsx $ip.exe
		$appLaunched = 1
		break
}

if ($appLaunched -eq 0) { 
	& "C:\Windows\System32\Shutdown" "/l", "/f"
	exit
}